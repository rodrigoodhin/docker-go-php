FROM ubuntu:20.04
LABEL maintainer="Rodrigo Odhin"

ARG DEBIAN_FRONTEND=noninteractive

# Install system libs, apache2, mysql-client, php and go
RUN apt-get update
RUN apt-get install -y openssh-server vim curl git sudo gpg
RUN apt-get install -y apache2 apache2-utils php7.4
RUN apt-get install -y mysql-client php-mysqlnd
RUN wget https://go.dev/dl/go1.17.7.linux-amd64.tar.gz
RUN rm -rf /usr/local/go && tar -C /usr/local -xzf go1.17.7.linux-amd64.tar.gz
RUN rm -rf go1.17.7.linux-amd64.tar.gz
RUN apt-get clean

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
COPY html /var/www/html

RUN mkdir /var/run/sshd

RUN echo 'root:root' |chpasswd
RUN sed -ri 's/^PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config
RUN echo 'Banner /etc/banner' >> /etc/ssh/sshd_config

COPY etc/banner /etc/

RUN useradd -ms /bin/bash app
RUN adduser app sudo
RUN echo 'app:app' |chpasswd

USER app

RUN echo "export PATH=$PATH:/usr/local/go/bin" >> ~/.bashrc

USER root

EXPOSE 22
EXPOSE 80

RUN mkdir /Users

RUN mkdir /scripts
COPY scripts/entrypoint.sh /scripts/entrypoint.sh
RUN ["chmod", "777", "/scripts/entrypoint.sh"]
ENTRYPOINT ["/scripts/entrypoint.sh"]
